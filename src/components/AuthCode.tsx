import { useEffect } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { useAuth } from "../hooks/useAuth";

export const AuthCode = () => {
  const location = useLocation()
  const navigate = useNavigate()
  const { handleCallback, loading } = useAuth()

  useEffect(() => {
      if (loading) {
          return;
      }
      const searchParams = new URLSearchParams(location.search);
      const code = searchParams.get('code');
      if (code) {
          handleCallback(code).then(() => {
              navigate('/home');
          });
      }
  }, [location, navigate, handleCallback, loading]);

  return (
    <div>
      <h1>AuthCode</h1>
    </div>
  );
}