import { useState } from 'react';
import keycloakConfig from '../keycloack-config.json';
import axios from 'axios';
import { jwtDecode } from 'jwt-decode';
import { useLocalStorage } from './useLocalStorage';

export const useAuth = () => {
  const [loading, setLoading] = useState(false);
  const { setValue, removeValue, getValue } = useLocalStorage()

  const openLoginPage = () => {
    const redirectUri = `${keycloakConfig.authServerUrl}/realms/${keycloakConfig.realm}/protocol/openid-connect/auth?client_id=${keycloakConfig.clientId}&response_type=code&redirect_uri=${keycloakConfig.redirectUri}`;
    window.location.href = redirectUri;
  }


  const handleCallback = async (code: string) => {
    const tokenUrl = `${keycloakConfig.authServerUrl}/realms/${keycloakConfig.realm}/protocol/openid-connect/token`;

    const params = new URLSearchParams();
    params.append('grant_type', 'authorization_code');
    params.append('code', code);
    params.append('redirect_uri', keycloakConfig.redirectUri);
    params.append('client_id', keycloakConfig.clientId);
    params.append('client_secret', keycloakConfig.clientSecret);

    try {
      setLoading(true);
      const response = await axios.post(tokenUrl, params);
      const { access_token, refresh_token } = response.data;
      console.log('response', response.data);
      const decodedToken = jwtDecode(access_token);
      console.log("decodedToken:", decodedToken);
      setValue('access_token', access_token);
      setValue('refresh_token', refresh_token);
      setValue('user_info', JSON.stringify(decodedToken));
    } catch (error) {
        console.error('Token exchange failed:', error);
    }
    setLoading(false);
  }

  const logout = () => {
    setLoading(true);
    removeValue('access_token');
    removeValue('refresh_token');
    removeValue('user_info');
    const logoutUrl = `${keycloakConfig.authServerUrl}/realms/${keycloakConfig.realm}/protocol/openid-connect/logout?post_logout_redirect_uri=http://localhost:3000/login&client_id=${keycloakConfig.clientId}`;
    window.location.href = logoutUrl;
  };

  const getUserInfo = () => {
    const userInfo = localStorage.getItem('user_info')
    const userInfoObj = userInfo ? JSON.parse(userInfo) : null
    console.log(`userInfoObj: ${userInfoObj}`);
    return userInfo ? userInfoObj : null;
  };

  const isAuthenticated = () => {
      const token = getValue('access_token');
      console.log(`access token: ${token}`);
      return !!token;
  };

  const isTokenExpired = (token: string) => {
      if (!token) return true;
      try {
          const { exp } = jwtDecode(token);
          return exp ? exp * 1000 < Date.now() : true;
      } catch (error) {
          return true;
      }
  };

  return { isAuthenticated, loading, handleCallback, openLoginPage, logout, getUserInfo };
}