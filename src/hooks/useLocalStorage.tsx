import { useCallback } from "react";

export const useLocalStorage = () => {

  const getValue = useCallback((key: string, defaultValue?: string) => {
    try {
      const item = window.localStorage.getItem(key);
      return item
        ? item
        : defaultValue ? defaultValue : '';
    } catch (error) {
      console.log(`Error getting item from localStorage: ${error}`);
      return defaultValue;
    }
  }, [])

  const setValue = useCallback((key: string, value: string) => {
    try {
      window.localStorage.setItem(key, value);
    } catch (error) {
      console.log(`Error setting item in localStorage: ${error}`);
    }
  }, [])

  const removeValue = useCallback((key: string) => {
    try {
      window.localStorage.removeItem(key);
    } catch (error) {
      console.log(`Error removing item from localStorage: ${error}`);
    }
  }, [])

  return { getValue, setValue, removeValue };
}